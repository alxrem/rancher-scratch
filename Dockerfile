FROM alpine AS builder
COPY noop.asm /
RUN apk update               \
    && apk add nasm binutils \
    && nasm -felf64 noop.asm \
    && ld noop.o -o noop     \
    && strip noop

FROM scratch
LABEL maintainer="Alexey Remizov <alexey@remizov.org>"
COPY --from=builder noop /
CMD ["/noop"]
